import React from "react";
import "semantic-ui-css/semantic.min.css";

import Layout from "./components/Layout";

export const App = () => {
   return <Layout />;
};
