import React, { useState } from "react";

import { Button, Card, Form, Grid, GridColumn, Header, Icon, Message, Segment } from "semantic-ui-react";

import Api from "../utils/Api";
import Results from "./Results";
import Mapa from "./Mapa";

const Layout = () => {
   const [ciudad, setCiudad] = useState("");
   const [data, setData] = useState({}); //{}vamos aguardar un objeto
   const [showResults, setShowResults] = useState(false); //{}vamos a guardar un objeto
   const [message, setMessage] = useState("Campo Vacío.");

   const inputHandler = (e) => {
      //console.log(e.target.value);
      const { value } = e.target;
      //console.log(value);
      setCiudad(value);
   };

   const onClikHandler = () => {
      //console.log(ciudad);
      fetchData(ciudad);
   };

   const fetchData = (ciudad) => {
      if (ciudad === "") {
         Api.getWeather(ciudad).catch((err) => {
            setShowResults(false);
            setMessage("Campo Vacío.");
         });
      } else {
         Api.getWeather(ciudad)
            .then((resp) => {
               console.log(resp.data);
               setData(resp.data);
               setShowResults(true);
            })
            .catch((err) => {
               console.log("ERROR: ", err);
               setShowResults(false);
               setMessage("ERROR: No se encontró la ciudad.");
            });
      }
   };

   const onClean = () => {
      setCiudad("");
      document.getElementById("txtCiudad").value = "";
      fetchData("");
   };

   return (
      <>
         <Grid textAlign="center" style={{ height: "90vh" }} verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 450 }}>
               <Header as="h1" color="blue" textAlign="center">
                  Search for Weather
               </Header>
               <Segment piled color="red">
                  <Grid>
                     <GridColumn>
                        <Form>
                           <Grid celled>
                              <Grid.Row>
                                 <Grid.Column width={13}>
                                    <Form.Input placeholder="City" id="txtCiudad" onChange={inputHandler} />
                                 </Grid.Column>
                                 <Grid.Column width={3}>
                                    <Button icon="eraser" onClick={onClean} />
                                 </Grid.Column>
                              </Grid.Row>
                           </Grid>
                           <Button
                              animated="vertical"
                              color="green"
                              className="submit"
                              type="submit"
                              onClick={onClikHandler}
                           >
                              <Button.Content visible>Search</Button.Content>
                              <Button.Content hidden>
                                 <Icon name="search"></Icon>
                              </Button.Content>
                           </Button>
                        </Form>
                     </GridColumn>
                  </Grid>
               </Segment>
               {showResults && <Results data={data} />}
               {showResults && <Mapa nombre={data.name} lon={data.coord.lon} lat={data.coord.lat} />}
               {/* {!showResults && (
                  <Message color="red">
                     <Message.Header>Aviso:</Message.Header>
                     <Message.Content>No hay información</Message.Content>
                  </Message>
               )} */}
               {!showResults && (
                  <Message color="red">
                     <Message.Header>Aviso:</Message.Header>
                     <Message.Content>{message}</Message.Content>
                  </Message>
               )}
            </Grid.Column>
         </Grid>
      </>
   );
};

export default Layout;
