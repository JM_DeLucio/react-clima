import React from "react";
import { ComposableMap, Geographies, Geography, Markers, Marker, countries } from "react-simple-maps";

const geoUrl = "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const Mapa = ({ nombre, lon, lat }) => {
   //console.log(nombre, lon, lat);
   return (
      <>
         <ComposableMap>
            <Geographies geography={geoUrl}>
               {({ geographies }) =>
                  geographies.map((geo) => <Geography key={geo.rsmKey} geography={geo} fill="#DDD" stroke="#FFF" />)
               }
            </Geographies>
            <Marker coordinates={[lon, lat]}>
               <circle r={8} fill="#F53" />
            </Marker>
         </ComposableMap>
      </>
   );
};

export default Mapa;
