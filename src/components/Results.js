import React, { useState } from "react";

import { Button, Card, CardHeader, Image } from "semantic-ui-react";

import ModalEjemplo from "./ModalEjemplo";
import ModalEjemplo2 from "./ModalEjemplo2";

const Results = ({ data }) => {
   //console.log(data.cod);
   const [state, setState] = useState(false);

   const handlerModal = () => {
      setState(!state);
   };

   //const { data } = props;//forma larga
   //console.log(data);
   return (
      <>
         <Card fluid color="green">
            <Card.Content>
               <Image size="mini" src={"http://openweathermap.org/img/w/" + data.weather[0].icon + ".png"}></Image>
               <CardHeader>{data.name}</CardHeader>
               <Card.Description>
                  Temperatura: <strong>{data.main.temp}</strong>°C
               </Card.Description>
               <Card.Meta>Max: {data.main.temp_max}</Card.Meta>
               <Card.Meta>Min: {data.main.temp_min}</Card.Meta>
            </Card.Content>
            <Card.Content extra>
               <Button onClick={handlerModal}>
                  <Button.Content visible>Mas información</Button.Content>
               </Button>
               <ModalEjemplo2
                  state={state}
                  setState={setState}
                  data_contry={data.sys.country}
                  data_name={data.name}
                  weather_description={data.weather[0].description}
                  weather_main={data.weather[0].main}
                  wind_deg={data.wind.deg}
                  wind_gust={data.wind.gust}
                  wind_speed={data.wind.speed}
               />
               <Card.Content>
                  {
                     // <ModalEjemplo
                     // data_contry={data.sys.country}
                     // data_name={data.name}
                     // weather_description={data.weather[0].description}
                     // weather_main={data.weather[0].main}
                     // wind_deg={data.wind.deg}
                     // wind_gust={data.wind.gust}
                     // wind_speed={data.wind.speed}
                     // />
                  }
               </Card.Content>
            </Card.Content>
         </Card>
      </>
   );
};

export default Results;
