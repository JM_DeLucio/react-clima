import React from "react";
import { Button, Modal, List } from "semantic-ui-react";

const ModalEjemplo2 = ({
   state,
   setState,
   data_contry,
   data_name,
   weather_description,
   weather_main,
   wind_deg,
   wind_gust,
   wind_speed,
}) => {
   console.log(state);
   return (
      <>
         <Modal centered={false} open={state} onClose={() => setState(false)}>
            <Modal.Header>El Clima en {data_name} es:</Modal.Header>
            <Modal.Content>
               <Modal.Description>
                  <List>
                     <List.Item icon="sun" content={weather_main} />
                     <List.Item icon="cloud" content={weather_description} />
                     <br></br>
                     Descriptión del viento
                     <br></br>
                     <List.Item icon="cloudversify" content={"Grado del viento: " + wind_deg} />
                     {wind_gust !== undefined && (
                        <List.Item icon="cloudversify" content={"Ráfaga del viento: " + wind_gust} />
                     )}
                     <List.Item icon="cloudversify" content={"Velocidad del viento: " + wind_speed} />
                  </List>
               </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
               <Button onClick={() => setState(false)}>OK</Button>
            </Modal.Actions>
         </Modal>
      </>
   );
};

export default ModalEjemplo2;
