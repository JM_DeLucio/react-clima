import axios from "axios";

const urlApi = process.env.REACT_APP_URL;
const key = process.env.REACT_APP_KEY;

console.log(process.env);
const Api = {
   getWeather: async (city) => {
      const url = urlApi + city + "&appid=" + key;
      return await axios.get(url);
   },
};

export default Api;
